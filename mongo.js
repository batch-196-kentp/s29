// db.products.insertMany(

// 	[

// 		{

// 			"name":"Iphone x",

// 			"price": 30000,

// 			"isActive": true

// 		},

// 		{

// 			"name":"Samsung Galaxy S21",

// 			"price": 51000,

// 			"isActive": true

// 		},

// 		{

// 			"name":"Razer Blackshark V2X",

// 			"price": 2800,

// 			"isActive": false

// 		},

// 		{

// 			"name":"RAKK Gaming Mouse",

// 			"price": 1800,

// 			"isActive": true

// 		},

// 		{

// 			"name":"Razer Mechanical Keyboard",

// 			"price": 4000,

// 			"isActive": true

// 		}

// 	]

// )

//Query Operators
//Allows us to expand our queries and define conditions
//Instead of just looking for specific values


//$gt,$lt,$gte,$lte

//$gt - query operator which means greater than
db.products.find({price:{$gt:3000}})

//$lt - query operator which meansless than
db.products.find({price:{$lt:3000}})

//$gte - query operator which means greater than or equal to
db.products.find({price:{$gte:30000}})


//$lte - query operator which mean less than or equal to 
db.products.find({price:{$lte:2800}})

db.users.insertMany(
	[
		{
			"firstName": "Mary Jane",
			"lastName": "Watson",
			"email": "mjtiger@gmail.com",
			"password": "tigerjackpot15",
			"isAdmin": false
		},
		{
			"firstName": "Gwen",
			"lastName": "Stacy",
			"email": "stacyTech@gmail.com",
			"password": "stacyTech1991",
			"isAdmin": true
		},
		{
			"firstName": "Peter",
			"lastName": "Parker",
			"email": "peterWebDev@gmail.com",
			"password": "webdeveloperPeter",
			"isAdmin": true
		},
		{
			"firstName": "Jonah",
			"lastName": "Jameson",
			"email": "jjjameson@gmail.com",
			"password": "spideyisamenace",
			"isAdmin": false
		},
		{
			"firstName": "Otto",
			"lastName": "Octavius",
			"email": "ottoOctopi@gmail.com",
			"password": "docOck15",
			"isAdmin": false
		}
	]

)
//$regex - query operator which will allow us to find documents which will match the
//characters/pattern of the characters we are looking for.
//$regex - looks for documents with partial match and by default is case sensitive
db.users.find({firstName:{$regex: 'O'}})

//$options - used of our regex will be case insensitive
db.users.find({firstName:{$regex:'o',$options: '$i'}})

//You can also find for documents with partial matches
db.products.find({name:{$regex: 'phone',$options: '$i'}})

//find user whose email have the word web in it
db.users.find({email:{$regex: 'web', $options: '$i'}})


db.products.find({name:{$regex: 'razer',$options: '$i'}})
db.products.find({name:{$regex: '',$options: '$i'}})

//$or $and - logical operators works almost the same way as they were in JS.
//$or - allow us to have a logical operation to find for documents which can satisfy at least 1 of our conditions.

db.products.find({$or:[{name:{$regex: 'x', $options: '$i'}},{price:{$lte:10000}}]})
db.products.find({$or:[{name:{$regex: 'x', $options: '%i'}},{price: {$gte:30000}}]})

//$and - look/find documents that satisfies both conditions.
db.products.find({$and:[{name:{$regex: 'razer', $options: '$i'}},{price:{$gte:3000}}]})
db.products.find({$and:[{name:{$regex: 'x', $options: '%i'}},{price: {$gte:30000}}]})

db.users.find({$and:[{lastName:{$regex: 'w', $options: '$i'}},{isAdmin:false}]})
db.users.find({$and:[{firstName:{$regex: 'a', $options: '$i'}},{isAdmin:true}]})

//Field Projection - allow us to hide/show certain properties/field
//db.collection.find({query},{projection) - 0 means hide, 1 means show
db.users.find({},{_id:0,password:0})

db.users.find({isAdmin:true},{_id:0,email:1})
//_id field must be explicitly hidden.
//WE can also just pick which fields to show
db.users.find({isAdmin:false},{firstName:1,lastName:1})


db.products.find({price:{$gte:10000}},{_id:0,name:1,price:1})




db.courses.insertMany(
	[
		{
			"name": "HTML Basics",
			"price": 20000,
			"isActive": true,
			"instructor": "Sir Alvin"
		},
		{
			"name": "CSS 101 + Flexbox",
			"price": 21000,
			"isActive": true,
			"instructor": "Sir Alvin"
		},
		{
			"name": "Javascript 101",
			"price": 32000,
			"isActive": true,
			"instructor": "Ma'am Tine"
		},
		{
			"name": "Git 101, ID and CLI",
			"price": 19000,
			"isActive": false,
			"instructor": "Ma'am Tine"
		},
		{
			"name": "React.Js 101",
			"price": 25000,
			"isActive": true,
			"instructor": "Ma'am Miah"
		}
	]
)
               
db.courses.find({$and:[{instructor: 'Sir Alvin'},{price:{$gte:20000}}]},{_id:0,name:1,price:1})
db.courses.find({$and:[{instructor: "Ma'am Tine"},{isActive:false}]},{_id:0,name:1,price:1})
db.courses.find({$and:[{name:{$regex: 'r', $options: '$i'}},{price:{$lte:25000}}]})

db.courses.updateMany({price:{$lt:21000}},{$set:{isActive: false}})
db.courses.deleteMany({price:{$gte:25000}})
